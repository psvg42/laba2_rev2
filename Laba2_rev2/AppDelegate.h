//
//  AppDelegate.h
//  Laba2_rev2
//
//  Created by vlutsyk on 11/23/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
