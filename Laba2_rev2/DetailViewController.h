//
//  DetailViewController.h
//  Laba2_rev2
//
//  Created by vlutsyk on 11/23/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic)  Group* group;
@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
