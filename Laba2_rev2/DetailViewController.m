//
//  DetailViewController.m
//  Laba2_rev2
//
//  Created by vlutsyk on 11/23/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()<UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate>
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setGroup:(Group *)group
{
    if (_group!= group) {
        _group = group;
        
        // Update the view.
        [self configureView];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.group.students count];
}

- (void)configureView
{
    // Update the user interface for the detail item.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    Student *student = self.group.students[indexPath.row];
    cell.textLabel.text = student.fio;
    cell.detailTextLabel.text = student.phoneNumber;
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editStudent:(id)sender {
    self.table.editing = !self.table.editing;
}


- (IBAction)addStudent:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add new Student"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Add", nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alert textFieldAtIndex:0].placeholder = @"FIO";
    [[alert textFieldAtIndex:1] setSecureTextEntry:NO];
    [alert textFieldAtIndex:1].placeholder = @"Phone Number";
    [alert show];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.group.students removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        UITextField *fio = [alertView textFieldAtIndex:0];
        UITextField *phoneNumber = [alertView textFieldAtIndex:1];
        if ([fio.text length]) {
            Student* student = [[Student alloc]initWithFIO:fio.text phoneNumber:phoneNumber.text];
            [self.group addStudent:student];
        }
        [self.table reloadData];
    }
}

@end
