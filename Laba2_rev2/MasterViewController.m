//
//  MasterViewController.m
//  Laba2_rev2
//
//  Created by vlutsyk on 11/23/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "Group.h"

@interface MasterViewController () <UIAlertViewDelegate>
{
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;

    //UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    //self.navigationItem.rightBarButtonItem = addButton;
    
    if (!_objects) {

    //create 5 group with students
        NSMutableArray* groups = [[NSMutableArray alloc]init];
        for (int i =0; i<5; i++) {
            Group* group = [[Group alloc]initWithTitle:[NSString stringWithFormat:@"Mif-%i1", i+1]];
            int numOfStudent = arc4random() % 10;
            for (int j=0; j<numOfStudent; j++) {
                Student* stud = [[Student alloc]initWithFIO:[NSString stringWithFormat:@"Student%i%i", i+1,j+1] phoneNumber:[NSString stringWithFormat:@"Phone%i%i", i+1,j+1]];
                [group addStudent:stud];
            }
            [groups addObject:group];
        }
        _objects = groups;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender group:(Group*)group
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Group *group = _objects[indexPath.row];
    cell.textLabel.text = group.groupTitle;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Group *group = _objects[indexPath.row];
        [[segue destinationViewController] setGroup:group];
    }
}

- (IBAction)editButtonPressed:(id)sender {
    self.tableView.editing = !self.tableView.editing;
}


- (IBAction)addButtonPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add new Group"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Add", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        UITextField *textfield = [alertView textFieldAtIndex:0];
        if ([textfield.text length]) {
            Group* newGroup = [[Group alloc]initWithTitle:textfield.text];
            [_objects addObject:newGroup];
        }
        [self.tableView reloadData];
        //UITextField *textfield = [alertView textFieldAtIndex:0];
        //NSLog(@"%@", textfield.text);
        //add
    }
}


@end
