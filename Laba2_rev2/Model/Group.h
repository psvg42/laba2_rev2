//
//  Group.h
//  Laba2
//
//  Created by vlutsyk on 11/22/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@interface Group : NSObject

@property (assign) int groupId;
@property (strong) NSString *groupTitle;
@property (strong) NSMutableArray *students;

-(id) initWithTitle:(NSString*) title;
-(void) addStudent:(Student*)student;

@end