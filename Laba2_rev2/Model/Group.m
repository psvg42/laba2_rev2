//
//  Group.m
//  Laba2
//
//  Created by vlutsyk on 11/22/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import "Group.h"

@implementation Group

-(id) initWithTitle:(NSString*) title {
    if (self = [super init]) {
        self.groupTitle = title;
    }
    return self;
}
-(void) addStudent:(Student*)student {
    if (!_students) {
        _students = [[NSMutableArray alloc]init];
    }
    [_students addObject:student];
}

@end
