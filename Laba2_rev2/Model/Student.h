//
//  Student.h
//  Laba2
//
//  Created by vlutsyk on 11/22/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject

@property (assign) int studentId;
@property (strong) NSString *fio;
@property (strong) NSString *phoneNumber;

-(id) initWithFIO:(NSString*)fio phoneNumber:(NSString*)phoneNumber;

@end
