//
//  Student.m
//  Laba2
//
//  Created by vlutsyk on 11/22/14.
//  Copyright (c) 2014 univer. All rights reserved.
//

#import "Student.h"

@implementation Student

-(id) initWithFIO:(NSString*)fio phoneNumber:(NSString*)phoneNumber {
    if (self = [super init]) {
        self.fio = fio;
        self.phoneNumber = phoneNumber;
    }
    return self;
}

@end
